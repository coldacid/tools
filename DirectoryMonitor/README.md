DirectoryMonitor
================

Console application for monitoring file system events.

This app is pretty much a wrapper around System.IO.FileSystemWatcher, with the
ability to monitor multiple different directories. It supports the ability to
filter what it monitors by glob (i.e. *.xml, etc.) but will use the same
filter for all directories monitored.

Usage: `DirectoryMonitor.exe [OPTIONS]+ path [path]+`

Options:
  -f, --filter=VALUE         filter to file names matching the given pattern

