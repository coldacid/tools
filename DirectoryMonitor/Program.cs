﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Options;

namespace DirectoryMonitor
{
    class Program
    {
        private static string _appName;

        private static int Main(string[] args)
        {
            _appName = Path.GetFileNameWithoutExtension(Process.GetCurrentProcess().MainModule.FileName);

            var filter = string.Empty;

            var showHelp = false;

            var opts = new OptionSet()
            {
                {"f|filter=", "filter to file names matching the given pattern", v => filter = v},
                {"h|help", "show this message and exit", v => showHelp = v != null}
            };

            List<string> paths;
            try
            {
                paths = opts.Parse(args);
            }
            catch (OptionException ex)
            {
                ErrorMessage(ex.Message);
                return 1;
            }

            if (showHelp)
            {
                ShowHelp(opts);
                return 0;
            }

            if (paths.Count == 0)
            {
                ErrorMessage("Missing path for monitoring");
                return 2;
            }

            var keepRunning = true;
            Console.CancelKeyPress += (s, e) =>
            {
                Console.Error.WriteLine("[{0:HH:mm:ss.fff}] Caught cancel signal, exiting", DateTime.Now);

                e.Cancel = true;
                keepRunning = false;
            };

            try
            {
                var watchers = !string.IsNullOrWhiteSpace(filter)
                    ? paths.ToDictionary(p => p, p => new FileSystemWatcher(p, filter))
                    : paths.ToDictionary(p => p, p => new FileSystemWatcher(p));

                foreach (var w in watchers.Values)
                {
                    w.Changed += FileSystemWatcher_OnEvent;
                    w.Created += FileSystemWatcher_OnEvent;
                    w.Deleted += FileSystemWatcher_OnEvent;
                    w.Renamed += FileSystemWatcher_OnRenamed;
                    w.Error += FileSystemWatcher_OnError;

                    w.IncludeSubdirectories = true;
                    w.EnableRaisingEvents = true;

                    Console.WriteLine("[{0:HH:mm:ss.fff}] Monitoring enabled for {1}", DateTime.Now,
                        w.Path + Path.DirectorySeparatorChar + w.Filter);
                }

                // ReSharper disable once LoopVariableIsNeverChangedInsideLoop
                while (keepRunning) { }

                return 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("[{0:HH:mm:ss.fff}] FATAL: {1} ({2})", DateTime.Now, ex.Message, ex.GetType().Name);
                return 3;
            }
        }

        private static void FileSystemWatcher_OnRenamed(object sender, RenamedEventArgs e)
        {
            Console.WriteLine("[{0:HH:mm:ss.fff}] {1}: {2} -> {3}", DateTime.Now, e.ChangeType, e.OldFullPath, e.FullPath);
        }

        private static void FileSystemWatcher_OnError(object sender, ErrorEventArgs e)
        {
            var watcher = sender as FileSystemWatcher;
            Debug.Assert(watcher != null);

            Console.Error.WriteLine("[{0:HH:mm:ss.fff}] ERROR: {1} ({2})\n\tAffects: {3}", DateTime.Now, e.GetException().Message, 
                e.GetException().GetType().Name, watcher.Path + Path.DirectorySeparatorChar + watcher.Filter);
        }

        private static void FileSystemWatcher_OnEvent(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("[{0:HH:mm:ss.fff}] {1}: {2}", DateTime.Now, e.ChangeType, e.FullPath);
        }

        private static void ErrorMessage(string message)
        {
            Console.Write("{0}: ", _appName);
            Console.WriteLine(message);
            Console.WriteLine("Try `{0} --help` for more information.", _appName);
        }

        private static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("Usage: {0} [OPTIONS]+ path [path]+", _appName);
            Console.WriteLine("Monitor file and directory changes for a given path.");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }
    }
}
